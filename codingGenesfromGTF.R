if(!require(optparse)){
    install.packages('optparse')
}
require(data.table)


option_list = list(
  make_option(c("-a", "--annotation"), action="store", default=NA, type='character',
              help="annoation file in gtf format"),
  make_option(c("-f", "--file"), action="store", default=NA, type='character',
              help="data format with matching gene names as column headers"),
  make_option(c("-c", "--codingGenes"), action="store", default=NA, type='character',
              help="coding genes output file"),
  make_option(c("-o", "--output.file"), action="store", default=NA, type='character',
              help="The output file")
)
opt = parse_args(OptionParser(option_list=option_list))

#test parameters
#opt<-NULL
#opt$annotation<-'/home/anne/Documents/featurecloud/data/genome/gencode.v22.annotation.gtf'
#opt$file <- '/home/anne/Documents/featurecloud/data/TCGA/htseq/output_transposed.txt'
#opt$codingGenes<- '/home/anne/Documents/featurecloud/data/TCGA/genome/coding_genes.txt'#
#opt$output.file<-'/home/anne/Documents/featurecloud/data/TCGA/htseq/output_coding_genes.txt'

# read annoation
if(!is.na(opt$annotation)){
require(GenomicFeatures)
gft<-makeTxDbFromGFF(opt$annotation)
#get coding genes and transform into data table
codingGenes<-cdsBy(gft, by='gene')
codingGenes<-as.data.table(names(codingGenes))
fwrite(codingGenes, file=opt$codingGenes, sep='\t')
}else{
  codingGenes<-fread(opt$codingGenes)
}

if(!is.na(opt$file)){
# read data and extract relevant columns
data<-fread(opt$file)
indices<- which(colnames(data) %in% codingGenes$V1)
data<-data[,indices, with=F]
fwrite(data,file = opt$output.file, sep='\t' )
}