#!/bin/bash
# This script gunzips files in the second subfolder

for dir in $(ls)
do
	echo $dir
	cd $dir
	for file in $(ls)
	do
		echo $file
		gunzip $file
	done
	cd ..
done
