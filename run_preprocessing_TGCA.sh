#!/bin/bash
# $1 Regex for files
# S2 Metadatafile = Sample table
Rscript ../../../featurecloud-test/data_preprocessing/organize_data.R -s $1 -o 'output.txt'
Rscript ../../../featurecloud-test/data_preprocessing/TCGA_rename_columns.R -f 'output.txt' -m $2 -o 'output_named.txt'
Rscript ../../../featurecloud-test/data_preprocessing/transpose_data.R -f output_named.txt -o output_transposed.txt
rm output_named.txt
rm output.txt
rm final_*

